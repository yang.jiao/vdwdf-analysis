# Analysis and visualization tools for vdW-DF project

### Yang Jiao, Apr. 2020

This directory contains scripts for analysis and visualization in vdW-DF project, and examples from our publications.

## Partition of XC contributions to binding in solids

### [J. Phys.: Condens. Matter 2020](https://doi.org/10.1088/1361-648X/ab8250)