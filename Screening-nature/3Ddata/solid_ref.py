'''

The following contains a database of strong bonded solid
Data for the strong bonded solid data base are from (a0 (\AA), B0 (GPa), Ea (kJ/mol/atom))
 L. Schimka, J. Harl, G. Kresse, Improved hybrid functional for solids: The HSEsol functional, J. Chem. Phys, 134, 024116 (2011) 

and 

(V0 (\AA^3), B0t (GPa),Ect (kJ/mol))
K. Lejaeghere, V. Van Speybroeck, G. Van Oost, S. Cottenier, Error Estimates for Solid-State Density-Functional Theory Predictions: An Overview by Means of the Ground-State Elemental Crystals, Critical Reviews in Solid State and Materials Sciences, 39:1-24, 2014

'''
import ase
from ase import Atoms
from ase.units import kcal, kJ
from ase.units import mol as Mol

atom_names = ['']

crystal_names = ['']

data = {
'C': {
    'name': 'C',
    'database': 'SBS',
    'symbols': 'CC',
    'lattice': 'Fd-3m',
    'a0': 3.553 ,
    'V0': 8.06 ,
    'B0': 454.7 ,
    'B0t': 55.6 ,
    'Ea': 728 ,
    'Ect': 719 ,
     },

'Si': {
    'name': 'Si' ,
    'database': 'SBS',
    'symbols': 'SiSi',
    'lattice': 'Fd-3m',
    'a0': 5.421 ,
    'V0': 19.82 ,
    'B0': 100.8 ,
    'B0t': 101.3 ,
    'Ea': 452 ,
    'Ect': 452 ,
     },

'Ge': {
    'name': 'Ge' ,
    'database': 'SBS',
    'symbols': 'GeGe',
    'lattice': 'Fd-3m',
    'a0': 5.644 ,
    'V0': 22.44 ,
    'B0': 77.3 ,
    'B0t': 79.4 ,
    'Ea': 378 ,
    'Ect': 375 ,
    },

'Sn': {
    'name': 'Sn' ,
    'database': 'SBS',
    'symbols': 'SnSn',
    'lattice': 'Fd-3m',
    'a0': 6.474 ,
    'V0': 33.97 ,
    'B0': 55.5 ,
    'B0t': 42.8 ,
    'Ea': 303 ,
    'Ect': 305 ,
    },

'SiC': {
    'name': 'SiC' ,
    'database': 'SBS',
    'symbols': 'SiC',
    'lattice': 'F-43m',
    'a0': 4.346 ,
    'B0': 229.1 ,
    'Ea': 625 ,
    },

'BN': {
    'name': 'BN' ,
    'database': 'SBS',
    'symbols': 'BN',
    'lattice': 'F-43m',
    'a0': 3.592 ,
    'B0': 410.2 , 
    'Ea': 652 ,
    }, 

'BP': {
    'name': 'BP' ,
    'database': 'SBS',
    'symbols': 'BP',
    'lattice': 'F-43m',
    'a0': 4.525 ,
    'B0': 168.0 ,
    'Ea': 496 ,
    },


'AlN': {
    'name': 'AlN' ,
    'database': 'SBS',
    'symbols': 'AlN',
    'lattice': 'F-43m',
    'a0': 4.368 ,
    'B0': 206.0 ,
    'Ea': 564 ,
    },


'AlP': {
    'name': 'AlP' ,
    'database': 'SBS',
    'symbols': 'AlP',
    'lattice': 'F-43m',
    'a0': 5.451 ,
    'B0': 87.4 ,
    'Ea' : 417 ,
    },


'AlAs': {
    'name': 'AlAs' ,
    'database': 'SBS',
    'symbols': 'AlAs',
    'lattice': 'F-43m',
    'a0': 5.649 ,
    'B0': 75.0 ,
    'Ea': 369 , 
    },


'GaN': {
    'name': 'GaN' ,
    'database': 'SBS',
    'symbols': 'GaN',
    'lattice': 'F-43m',
    'a0': 4.509 ,
    'B0': 213.7 ,
    'Ea': 439 ,
    },


'GaP': {
    'name': 'GaP' ,
    'database': 'SBS',
    'symbols': 'GaP',
    'lattice': 'F-43m',
    'a0': 5.439 ,
    'B0': 89.6 , 
    'Ea': 348 ,
    }, 

'GaAs': {
    'name': 'GaAs' ,
    'database': 'SBS',
    'symbols': 'GaAs',
    'lattice': 'F-43m',
    'a0': 5.640 ,
    'B0': 76.7 ,
    'Ea': 322 ,
    },


'InP': {
    'name': 'InP' ,
    'database': 'SBS',
    'symbols': 'InP',
    'lattice': 'F-43m',
    'a0': 5.858 ,
    'B0': 72.0 ,
    'Ea': 335 ,
    },


'InAs': {
    'name': 'InAs' ,
    'database': 'SBS',
    'symbols': 'InAs',
    'lattice': 'F-43m',
    'a0': 6.047 ,
    'B0': 58.6 ,
    'Ea': 297 ,
    },


'InSb': {
    'name': 'InSb' ,
    'database': 'SBS',
    'symbols': 'InSb',
    'lattice': 'F-43m',
    'a0': 6.473 ,
    'B0': 46.1 ,
    'Ea': 271 ,
    },

'LiH':{ 
    'name': 'LiH' ,
    'database': 'SBS',
    'symbols': 'LiH',
    'lattice': 'Fm-3m',
    'a0': 3.979 ,
    'B0': 40.1 ,
    'Ea': 240 ,
    },

'LiF':{ 
    'name': 'LiF' ,
    'database': 'SBS',
    'symbols': 'LiF',
    'lattice': 'Fm-3m',
    'a0': 3.972 ,
    'B0': 76.3 ,
    'Ea': 430 ,
    },

'LiCl':{ 
    'name': 'LiCl' ,
    'database': 'SBS',
    'symbols': 'LiCl',
    'lattice': 'Fm-3m',
    'a0': 5.070 ,
    'B0': 38.7 ,
    'Ea': 346 ,
    },

'NaF':{ 
    'name': 'NaF' ,
    'database': 'SBS',
    'symbols': 'NaF',
    'lattice': 'Fm-3m',
    'a0': 4.582 ,
    'B0': 53.1 ,
    'Ea': 383 ,
    },

'NaCl':{ 
    'name': 'NaCl' ,
    'database': 'SBS',
    'symbols': 'NaCl',
    'lattice': 'Fm-3m',
    'a0': 5.569 ,
    'B0': 27.6 ,
    'Ea': 322 ,
    },

'MgO':{ 
    'name': 'MgO' ,
    'database': 'SBS',
    'symbols': 'MgO',
    'lattice': 'Fm-3m',
    'a0': 4.189 ,
    'B0': 169.8 ,
    'Ea': 502 ,
    },

'Li':{ 
    'name': 'Li' ,
    'database': 'SBS',
    'symbols': 'Li',
    'lattice': 'Im-3m',
    'a0': 3.453 ,
    'V0': 20.41 ,
    'B0': 13.9 ,
    'B0t': 13.1 ,
    'Ea': 160 , 
    'Ect': 161 , 
    },

'Na':{ 
    'name': 'Na' ,
    'database': 'SBS',
    'symbols': 'Na',
    'lattice': 'Im-3m',
    'a0': 4.214 ,
    'V0': 37.18 ,
    'B0': 7.7 ,
    'B0': 7.9 ,
    'Ea': 108 ,
    'Ect': 108 ,
    },

'Al':{ 
    'name': 'Al' ,
    'database': 'SBS',
    'symbols': 'Al',
    'lattice': 'Fm-3m',
    'a0': 4.018 ,
    'V0': 16.27 ,
    'B0': 82.0 ,
    'B0t': 77.1 ,
    'Ea': 331 ,
    'Ect': 331 ,
    },

'K':{ 
    'name': 'K' ,
    'database': 'SBS',
    'symbols': 'K',
    'lattice': 'Im-3m',
    'V0': 72.20 ,
    'B0t': 3.8 ,
    'Ect': 91.0 , 
    },

'Ca':{ 
    'name': 'Ca' ,
    'database': 'SBS',
    'symbols': 'Ca',
    'lattice': 'Fm-3m',
    'V0': 42.95 ,
    'B0t': 15.9 ,
    'Ect': 180
    },

'Rb':{ 
    'name': 'Rb' ,
    'database': 'SBS',
    'symbols': 'Rb',
    'lattice': 'Im-3m',
    'V0': 89.16 ,
    'B0t': 3.6,
    'Ect': 82.7 ,
    },

'Sr':{ 
    'name': 'Sr' ,
    'database': 'SBS',
    'symbols': 'Sr',
    'lattice': 'Fm-3m',
    'V0': 55.60 ,
    'B0t': 12.0 ,
    'Ect': 167 ,
    },

'Cs':{ 
    'name': 'Cs' ,
    'database': 'SBS',
    'symbols': 'Cs',
    'lattice': 'Im-3m',
    'V0': 110.32 ,
    'B0t': 2.3 ,
    'Ect': 78.0 ,
    },

'Ba':{ 
    'name': 'Ba' ,
    'database': 'SBS',
    'symbols': 'Ba',
    'lattice': 'Im-3m',
    'V0': 62.29 ,
    'V00': 62.99 ,
    'B0t': 10.6 ,
    'Ect': 184 ,
    },

'V':{ 
    'name': 'V' ,
    'database': 'SBS',
    'symbols': 'V',
    'lattice': 'Im-3m',
    'V0': 13.81 ,
    'B0t': 165.8 ,
    'Ect': 516 ,
    },

'Ni':{ 
    'name': 'Ni' ,
    'database': 'SBS',
    'symbols': 'Ni',
    'lattice': 'Fm-3m',
    'V0': 10.81 ,
    'B0t': 192.5 ,
    'Ect': 432 ,
    },

'Cu':{ 
    'name': 'Cu' ,
    'database': 'SBS',
    'symbols': 'Cu',
    'lattice': 'Fm-3m',
    'a0': 3.595 ,
    'V0': 11.65 ,
    'B0': 145.0 ,
    'B0t': 144.3 ,
    'Ea': 340 ,
    'Ect': 339 ,
    },

'Nb':{ 
    'name': 'Nb' ,
    'database': 'SBS',
    'symbols': 'Nb',
    'lattice': 'Im-3m',
    'V0': 17.97 ,
    'B0t': 173.2 ,
    'Ect': 733 ,
    },

'Mo':{ 
    'name': 'Mo' ,
    'database': 'SBS',
    'symbols': 'Mo',
    'lattice': 'Im-3m',
    'V0': 15.51 ,
    'B0t': 276.2 ,
    'Ect': 662 ,
    },

'Rh':{ 
    'name': 'Rh' ,
    'database': 'SBS',
    'symbols': 'Rh',
    'lattice': 'Fm-3m',
    'a0': 3.794 ,
    'V0': 13.57 ,
    'B0': 272.1 ,
    'B0t': 277.1 ,
    'Ea': 558 ,
    'Ect': 558 ,
    },

'Pd':{ 
    'name': 'Pd' ,
    'database': 'SBS',
    'symbols': 'Pd',
    'lattice': 'Fm-3m',
    'a0': 3.876 ,
    'V0': 14.56 ,
    'B0': 198.1 ,
    'B0t': 187.2 ,
    'Ea': 380 ,
    'Ect': 379 ,
    },

'Ag':{ 
    'name': 'Ag' ,
    'database': 'SBS',
    'symbols': 'Ag',
    'lattice': 'Fm-3m',
    'a0': 4.062 ,
    'V0': 16.85 ,
    'B0': 110.8 ,
    'B0t': 105.7 ,
    'Ea': 288 ,
    'Ect': 286 ,
    },

'Ta':{ 
    'name': 'Ta' ,
    'database': 'SBS',
    'symbols': 'Ta',
    'lattice': 'Im-3m',
    'V0': 17.93 ,
    'B0t': 202.7 ,
    'Ect': 784 ,
    },

'W':{ 
    'name': 'W' ,
    'database': 'SBS',
    'symbols': 'W',
    'lattice': 'Im-3m',
    'V0': 15.80 ,
    'B0t': 327.5 ,
    'Ect': 863 ,
    },

'Ir':{ 
    'name': 'Ir' ,
    'database': 'SBS',
    'symbols': 'Ir',
    'lattice': 'Fm-3m',
    'V0': 14.06 ,
    'B0t': 362.2 ,
    'Ect': 674 ,
    },

'Pt':{ 
    'name': 'Pt' ,
    'database': 'SBS',
    'symbols': 'Pt',
    'lattice': 'Fm-3m',
    'V0': 15.02 ,
    'B0t': 285.5 ,
    'Ect': 566 ,
    },

'Au':{ 
    'name': 'Au' ,
    'database': 'SBS',
    'symbols': 'Au',
    'lattice': 'Fm-3m',
    'V0': 16.82 ,
    'B0t': 182.0 ,
    'Ect': 370 ,
    },

'Ru':{
    'name': 'Ru',
    'symbols': 'RuRu',
    'lattice': 'P63mmc',
    'a0': 4.281/1.582,
    'covera': 1.582,
    },
'Rufcc':{
    'name': 'Rufcc',
    'symbols': 'Ru', 
    'lattice': 'Fm-3m',
    'a0': 3.831,   # Phys. Rev. B 48, 11685  
    'B0t': 0. ,
    'Ect': 0. ,
    },
}

def build_cry(item):
   chemsym=ase.Atoms(item['symbols']).get_chemical_symbols()
   sym=item['lattice']
   geo=[]
   
   if(sym=='Fd-3m'):     # diamond fcc
      geo.append([chemsym[0],0.,0.,0.])
      geo.append([chemsym[1],0.25,0.25,0.25])
   elif(sym=='F-43m'):   # zincblend fcc
      geo.append([chemsym[0],0.,0.,0.])
      geo.append([chemsym[1],0.25,0.25,0.25])
   elif(sym=='Fm-3m'):   # rocksalt fcc
      if(len(chemsym)==1): 
         geo.append([chemsym[0],0.,0.,0.])
      else:              
         geo.append([chemsym[0],0.,0.,0.])
         geo.append([chemsym[1],0.5,0.5,0.5])
   elif(sym=='Im-3m'):   # bcc 
      if(len(chemsym)==1): 
         geo.append([chemsym[0],0.,0.,0.])
      else:
         geo.append([chemsym[0],0.,0.,0.])
         geo.append([chemsym[1],0.5,0.5,0.5])
   else:
      print( 'Error: %s not implemented'%sym)
   return geo
   
def get_alat(item):
   chemsym=ase.Atoms(item['symbols']).get_chemical_symbols()
   sym=item['lattice']
   if ('a0' in item):
      alat=item['a0']
   elif ('V0' in item):
      if(sym=='Fd-3m' or sym=='F-43m' or sym=='Fm-3m'): 
         nfld=4.
      elif(sym=='Im-3m'):
         if(len(chemsym)==1): 
            nfld=2.
         elif(len(chemsym)==2): 
            nfld=1.
         else:  
            'Error wrong number of chemsymbols'
      else:
         print ('Error: %s not implemented'%sym)
      alat=(item['V0']*float(len(chemsym))*nfld)**(1./3.)
   else:
      print ('a0 V0 do not in data base for %s'%item)
   return alat

def get_B0ref(item):
   if ('B0' in item):
      B0ref=item['B0'] 
   elif ('B0t' in item):
      B0ref=item['B0t']
   else:
      print ('B0 do not in data base for %s'%item)
   return B0ref

def get_Earef(item):
   if ('Ea' in item):
      Earef=item['Ea']
   elif ('Ect' in item):
      Earef=item['Ect']
   else:
      print ('Ea do not in data base for %s'%item)
   Earef=Earef*kJ/Mol
   return Earef

def get_ibrav(item):
   chemsym=ase.Atoms(item['symbols']).get_chemical_symbols()
   sym=item['lattice']
   if(sym=='Fd-3m' or sym=='F-43m' or sym=='Fm-3m'): 
      ibrav=2  # fcc
   elif(sym=='Im-3m'):
      if(len(chemsym)==1): 
         ibrav=3 # bcc
      elif(len(chemsym)==2): 
         ibrav=1 # sc
      else:  
         'Error wrong number of chemsymbols'
   elif(sym=='P63mmc'):
      ibrav=4  # hcp
   else:
      print ('Error: %s not implemented'%sym)
   return ibrav
    
'''
for i in data:
   mol=data[i]
   print '%4s%12.3f'%( i, get_Earef(mol)),

'''
    
'''
MAD=0.
MARD=0.
icount=0
for i in data:
   mol=data[i]
   if('Ea' in mol and 'Ect' in mol):
      icount=icount+1
      MAD=MAD+abs(mol['Ect']-mol['Ea'])
      MARD=MARD+abs(mol['Ect']-mol['Ea'])/abs(mol['Ea'])
MAD=MAD/float(icount)*kJ/Mol
MARD=MARD/float(icount)
print icount, MAD, MARD

MAD=0.
MARD=0.
icount=0
for i in data:
   mol=data[i]
   if('B0' in mol and 'B0t' in mol):
      icount=icount+1
      MAD=MAD+abs(mol['B0t']-mol['B0'])
      MARD=MARD+abs(mol['B0t']-mol['B0'])/abs(mol['B0'])
      print i, mol['B0'], mol['B0t']
MAD=MAD/float(icount)
MARD=MARD/float(icount)
print icount, MAD, MARD
'''

