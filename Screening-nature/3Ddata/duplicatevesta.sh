#########################################
#  Si

cp Si-de-ecnl-uc.vesta Si-de-ecnl-nf-uc.vesta 
sed -i '' $'s/+91.81576765360094 Si-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Si-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' Si-de-ecnl-nf-uc.vesta

cp Si-de-ecnl-uc.vesta Si-de-ecnl-pvdW-uc.vesta 
sed -i '' $'s/+91.81576765360094 Si-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Si-bind-ecnl-pvdW-vdW-DF-cx-trispline.cube/g' Si-de-ecnl-pvdW-uc.vesta

cp Si-de-ecnl-uc.vesta Si-de-ecgc-uc.vesta 
sed -i '' $'s/+91.81576765360094 Si-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Si-bind-ecgc-PBE-trispline.cube/g' Si-de-ecgc-uc.vesta

cp Si-de-ecnl-uc.vesta Si-de-ec-alpha-uc.vesta 
sed -i '' 's/IMPORT_DENSITY 1/IMPORT_DENSITY 2/g' Si-de-ec-alpha-uc.vesta 
sed -i '' $'s/+91.81576765360094 Si-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Si-bind-eclda-vdW-DF-cx-trispline.cube\\\n+91.81576765360094 Na-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' Si-de-ec-alpha-uc.vesta


#########################################
# Na

cp Na-de-ecnl-vdW-DF-cx-uc.vesta Na-de-ecnl-nf-vdW-DF-cx-uc.vesta 
sed -i '' $'s/+91.81576765360094 Na-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Na-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' Na-de-ecnl-nf-vdW-DF-cx-uc.vesta

cp Na-de-ecnl-vdW-DF-cx-uc.vesta Na-de-ecnl-pvdW-vdW-DF-cx-uc.vesta 
sed -i '' $'s/+91.81576765360094 Na-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Na-bind-ecnl-pvdW-vdW-DF-cx-trispline.cube/g' Na-de-ecnl-pvdW-vdW-DF-cx-uc.vesta

cp Na-de-ecnl-vdW-DF-cx-uc.vesta Na-de-ecgc-PBE-uc.vesta 
sed -i '' $'s/+91.81576765360094 Na-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Na-bind-ecgc-PBE-trispline.cube/g' Na-de-ecgc-PBE-uc.vesta

cp Na-de-ecnl-vdW-DF-cx-uc.vesta Na-de-ec-alpha-vdW-DF-cx-uc.vesta 
sed -i '' 's/IMPORT_DENSITY 1/IMPORT_DENSITY 2/g' Si-de-ec-alpha-uc.vesta 
sed -i '' $'s/+91.81576765360094 Na-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 Na-bind-eclda-vdW-DF-cx-trispline.cube\\\n+91.81576765360094 Na-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' Na-de-ec-alpha-vdW-DF-cx-uc.vesta
#########################################
# W

cp W-de-ecnl-vdW-DF-cx-uc.vesta W-de-ecnl-nf-vdW-DF-cx-uc.vesta 
sed -i '' $'s/+91.81576765360094 W-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 W-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' W-de-ecnl-nf-vdW-DF-cx-uc.vesta

cp W-de-ecnl-vdW-DF-cx-uc.vesta W-de-ecnl-pvdW-vdW-DF-cx-uc.vesta 
sed -i '' $'s/+91.81576765360094 W-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 W-bind-ecnl-pvdW-vdW-DF-cx-trispline.cube/g' W-de-ecnl-pvdW-vdW-DF-cx-uc.vesta

cp W-de-ecnl-vdW-DF-cx-uc.vesta W-de-ecgc-PBE-uc.vesta 
sed -i '' $'s/+91.81576765360094 W-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 W-bind-ecgc-PBE-trispline.cube/g' W-de-ecgc-PBE-uc.vesta

cp W-de-ecnl-vdW-DF-cx-uc.vesta W-de-ec-alpha-vdW-DF-cx-uc.vesta 
sed -i '' 's/IMPORT_DENSITY 1/IMPORT_DENSITY 2/g' Si-de-ec-alpha-uc.vesta 
sed -i '' $'s/+91.81576765360094 W-bind-ecnl-vdW-DF-cx-trispline.cube/+91.81576765360094 W-bind-eclda-vdW-DF-cx-trispline.cube\\\n+91.81576765360094 W-bind-ecnl-nf-vdW-DF-cx-trispline.cube/g' W-de-ec-alpha-vdW-DF-cx-uc.vesta

