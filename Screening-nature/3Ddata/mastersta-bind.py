import sys
import numpy as np
import qe3d
from time import time
from datetime import timedelta

solname=sys.argv[1]
datatype_vdw= (['eclda','ecnl','exlda','exgc','tclda','tcnl'])
datatype_gc= (['eclda','ecgc','exlda','exgc','tclda','tcgc'])

##########################################################

def print_cube_bind(datatype,ixc):
   start=time()
   dt_mol=qe3d.qe3d('%s/%s.%s'%(solname,ixc,datatype))
   nat=dt_mol.nat
   ntyp=dt_mol.ntyp
   atm=dt_mol.atm
   ityp=dt_mol.ityp

   dr=.5
   at=dt_mol.get_at()
   r_mol=dt_mol.get_rbohr()
   grid_t=dt_mol.get_grid()
   print('Grid for cube file ready')
   end=time()
   print('Time elapsed %s'%(str(timedelta(seconds=int(end-start)))))

   bd_bind=-1.*dt_mol.bulkdata
   print('Bulk data for molecule ready')
   end=time()
   print('Time elapsed %s'%(str(timedelta(seconds=int(end-start)))))

   nat_ = 0
   r_mol_ = []
   for iat in range(nat):
      dt_atm=qe3d.qe3d(('Atom/'+'%s'+'-%s.%s')%(atm[ityp[iat]-1],ixc,datatype))
      r_atm=dt_atm.get_rbohr()

      for ix in ([-1,0,1]):
         if((dt_mol.tau[iat][0]+float(ix))<-dr \
         or (dt_mol.tau[iat][0]+float(ix))>1.+dr): continue
         for iy in ([-1,0,1]):
            if((dt_mol.tau[iat][1]+float(iy))<-dr \
            or (dt_mol.tau[iat][1]+float(iy))>1.+dr): continue
            for iz in ([-1,0,1]):
               if((dt_mol.tau[iat][2]+float(iz))<-dr \
               or (dt_mol.tau[iat][2]+float(iz))>1.+dr): continue
               nat_ = nat_ + 1
               r_mol_.append([])
               vec=r_mol[iat]
               vec[0]=vec[0]+float(ix)*at[0][0]+float(iy)*at[1][0]+float(iz)*at[2][0]
               vec[1]=vec[1]+float(ix)*at[0][1]+float(iy)*at[1][1]+float(iz)*at[2][1]
               vec[2]=vec[2]+float(ix)*at[0][2]+float(iy)*at[1][2]+float(iz)*at[2][2]
               r_mol_[-1] = vec
               vec=vec-r_atm[0]
               print(('atom %i shift %i %i %i')%(iat,ix,iy,iz))
               print('move the atom by ', vec)
               bd_atm=dt_atm.shift_interpolate(vec,grid_t)
               print('Bulk data for atom %i ready'%iat)
               end=time()
               print('Time elapsed %s'%(str(timedelta(seconds=int(end-start)))))
               bd_bind=bd_bind+bd_atm 
               """
               qe3d.savecube('%s-%i-%s-qe3d.cube'%(atm[ityp[iat]-1],iat,datatype),  \
                 'bulk data by tricubic interpolation', \
                 nat,([0.,0.,0.]),dt_mol.nr1x,dt_mol.nr1y,dt_mol.nr1z, \
                 at,atm,ityp,r_mol,bd_atm)
               """

   qe3d.savecube('%s-bind-%s-%s-trispline.cube'%(solname,datatype,ixc),  \
                 'bulk data by tricubic interpolation', \
                 nat_,([0.,0.,0.]),dt_mol.nr1x,dt_mol.nr1y,dt_mol.nr1z, \
                 at,atm,ityp,r_mol_,bd_bind)

   end=time()
   print('Time elapsed %s'%(str(timedelta(seconds=int(end-start)))))
   return

"""
ixc='vdW-DF-cx'
for datatype in datatype_vdw:
   print_cube_bind(datatype,ixc)

ixc='PBE'
for datatype in datatype_gc:
   print_cube_bind(datatype,ixc)
"""

print_cube_bind('ecnl','vdW-DF-cx')
