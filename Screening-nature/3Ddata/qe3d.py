############################################
# Read Quantum Espresso output 3D data file
# Shift and interpolate 3D data to a given grid
# 
#    Yang Jiao, May 2017, GPL
############################################

import numpy as np
import sys
from eqtools.trispline import Spline as trispline
from time import time
from datetime import timedelta


##################################################
#  Reorder the vector for CUBE file
###################################################
def bd_F2C(bulkdata_f,nx,ny,nz):
   bulkdata_3d=np.reshape(bulkdata_f,(nx,ny,nz),order='F')
   bulkdata_c=np.zeros(nx*ny*nz)
   i=0
   for ix in range(nx):
      for iy in range(ny):
         for iz in range(nz):
            bulkdata_c[i]=bulkdata_3d[ix][iy][iz]
            i=i+1

   return bulkdata_c

#################################################
# Quantum Espresso 3D data from file [ifilename]
# title
# nr1x, nr2x, nr3x, nr1, nr2, nr3, nat, ntyp
# ibrav, celldm
# if(ibrav==0) do i=1,3  (at(ipol,i), ipol=1,3)
# gcutm, dual, ecut, plot_num
# nt, atm(nt),zv(nt), nt=1,ntyp
# na, tau(ipol,na),ipol=1,3), ityp(na), na=1, nat
# plot(ir), ir=1,nr1x*nr2x*nr3x
# 
# variables:
#   title
#   nr1x, nr1y, nr1z, (npt,) nat, ntyp
#   ibrav, celldm
#   atm, zv
#   tau, ityp
#   bulkdata
#   
# functions:
#   get_at(self):
#   get_rbohr(self):
#   get_grid(self):
#   shift_interpolate(self,rshift,grid_t):
#################################################
class qe3d:
   def __init__(self,ifilename):
      finp=open(ifilename,'r')

      line=finp.readline()    
      qe3d.title=line

      line=finp.readline().split()
      self.nr1x=int(line[0])
      self.nr1y=int(line[1])
      self.nr1z=int(line[2])
      self.nat=int(line[6])
      self.ntyp=int(line[7])
      
      line=finp.readline().split()
      self.ibrav=int(line[0])
      self.celldm=np.zeros(6)
      for i in range(6):
         self.celldm[i]=float(line[i+1])
      if(self.ibrav==0):
         self.at_input=np.zeros((3,3))
         for ipol in range(3):      
            line=finp.readline().split() # at(ipol,i)
            self.at_input[ipol]=line[0:3]
      
      line=finp.readline().split()

      self.atm=['' for it in range(self.ntyp)]
      self.zv=np.zeros(self.ntyp)
      for it in range(self.ntyp):
         line=finp.readline().split()
         self.atm[it]=line[1]
         self.zv[it]=float(line[2])

      self.tau=np.zeros((self.nat,3))
      self.ityp=np.zeros((self.nat,), dtype=np.int)
      for ia in range(self.nat):
         line=finp.readline().split()
         for ipol in range(3):
            self.tau[ia][ipol]=float(line[ipol+1])
         self.ityp[ia]=int(line[4])

      self.npt=self.nr1x*self.nr1y*self.nr1z
      bulkdata_f=np.zeros(self.npt)
      line=finp.read().split()
      for i in range(self.npt):
         bulkdata_f[i]=float(line[i])
      self.bulkdata=bd_F2C(bulkdata_f,self.nr1x,self.nr1y,self.nr1z)

      finp.close()

   ############################
   # Adapted and modified from Quantum Espresso 6.1/Modules/latgen.f90
   ###########################
   def get_at(self):
      at=np.zeros((3,3))
      if(self.ibrav==0):     # user-supplied lattice vectors
         at=self.at_input*celldm[0]
      elif(self.ibrav==1):   # simple cubic lattice
         at[0][0]=self.celldm[0]
         at[1][1]=self.celldm[0]
         at[2][2]=self.celldm[0]
      elif(self.ibrav==2):   # fcc lattice
         term=self.celldm[0]/2.
         at[0][0]=-term
         at[0][2]=term
         at[1][1]=term
         at[1][2]=term
         at[2][0]=-term
         at[2][1]=term
      elif(self.ibrav==3):   # bcc lattice
         term=self.celldm[0]/2.
         at=term*np.ones((3,3))
         at[1][0]=-term
         at[2][0]=-term
         at[2][1]=-term
      elif(self.ibrav==4):   # hexagonal lattice
         at[0][0]=self.celldm[0]
         at[1][0]=-self.celldm[0]/2.
         at[1][1]=self.celldm[0]*np.sqrt(3.)/2.
         at[2][2]=self.celldm[0]*self.celldm[2]
      else:
         print('Error: ibrav = i not available'%(self.ibrav))
         sys.exit()

      return at

   ###########################################
   #  Atom coordinates in Bohr
   ##########################################
   def get_rbohr(self):
#      at=self.get_at()
      r=np.zeros((self.nat,3))
#      for ia in range(self.nat):
#         for ipol in range(3):
#            r[ia][ipol]=r[ia][ipol]+at[0][ipol]*self.tau[ia][0] \
#                                   +at[1][ipol]*self.tau[ia][1] \
#                                   +at[2][ipol]*self.tau[ia][2] 
      alat=self.celldm[0]
      r=alat*self.tau
      return r

   ############################################
   # Grid
   ############################################
   def get_grid(self):
      at=self.get_at()
      grid=np.zeros((self.npt,3))
      i=0
      for ix in range(self.nr1x):
         for iy in range(self.nr1y):
            for iz in range(self.nr1z):
               for ipol in range(3):
                  grid[i][ipol]=at[0][ipol]*float(ix)/float(self.nr1x) \
                               +at[1][ipol]*float(iy)/float(self.nr1y) \
                               +at[2][ipol]*float(iz)/float(self.nr1z)
               i=i+1
      return grid

   #############################################
   #  Shift and interpolate 
   ############################################
   def shift_interpolate(self,rshift,grid_t):
      start_si=time()
      at=self.get_at()
      if(at[0][1]**2+at[0][2]**2+at[1][0]**2+at[1][2]**2+at[2][0]**2+at[2][1]>0.): 
         print('Error: trispline interpolate not implemented for non cartesian coordinates')
      grid_x=np.linspace(0.+rshift[0],at[0][0]+rshift[0],self.nr1x,endpoint=False)
      grid_y=np.linspace(0.+rshift[1],at[1][1]+rshift[1],self.nr1y,endpoint=False)
      grid_z=np.linspace(0.+rshift[2],at[2][2]+rshift[2],self.nr1z,endpoint=False)
      # Check if it is needed to extend the grid
      ifextend=False
      bb1=np.amin(grid_t,axis=0)
      bb2=np.amax(grid_t,axis=0)
      kb1=np.zeros(3,dtype=int)
      kb2=np.zeros(3,dtype=int)
      dx=at[0][0]/float(self.nr1x)
      dy=at[1][1]/float(self.nr1y)
      dz=at[2][2]/float(self.nr1z)
      if(bb1[0]<grid_x.min()):
         ifextend=True
         kb1[0]=int(np.abs(grid_x.min()-bb1[0])/dx)+1
      if(bb2[0]>grid_x.max()):
         ifextend=True 
         kb2[0]=int(np.abs(bb2[0]-grid_x.max())/dx)+1
      if(bb1[1]<grid_y.min()):
         ifextend=True
         kb1[1]=int(np.abs(grid_y.min()-bb1[1])/dy)+1
      if(bb2[1]>grid_x.max()):
         ifextend=True 
         kb2[1]=int(np.abs(bb2[1]-grid_y.max())/dy)+1
      if(bb1[2]<grid_z.min()):
         ifextend=True
         kb1[2]=int(np.abs(grid_z.min()-bb1[2])/dz)+1
      if(bb2[2]>grid_z.max()):
         ifextend=True 
         kb2[2]=int(np.abs(bb2[2]-grid_z.max())/dz)+1

      bulkdata_f=np.reshape(self.bulkdata,(self.nr1x,self.nr1y,self.nr1z),order='C')
      if(ifextend):
         n1x=self.nr1x+kb1[0]+kb2[0]
         n1y=self.nr1y+kb1[1]+kb2[1]
         n1z=self.nr1z+kb1[2]+kb2[2]
         npt_ex=n1x*n1y*n1z
         bulkdata_ex=np.zeros(npt_ex)
         i=0
         for ix in range(-kb1[0],n1x-kb1[0]):
            iix=ix
            if(ix < 0): iix=ix+self.nr1x
            if(ix >= self.nr1x): iix=ix-self.nr1x
            for iy in range(-kb1[1],n1y-kb1[1]):
               iiy=iy
               if(iy < 0): iiy=iy+self.nr1y
               if(iy >= self.nr1y): iiy=iy-self.nr1y
               for iz in range(-kb1[2],n1z-kb1[2]):
                  iiz=iz
                  if(iz < 0): iiz=iz+self.nr1z
                  if(iz >= self.nr1z): iiz=iz-self.nr1z
                  bulkdata_ex[i]=bulkdata_f[iix][iiy][iiz]
                  i=i+1
         grid_x_ex=np.zeros(n1x)
         grid_y_ex=np.zeros(n1y)
         grid_z_ex=np.zeros(n1z)
         i=0
         for ix in range(-kb1[0],n1x-kb1[0]):
            grid_x_ex[i]=dx*float(ix)+rshift[0]
            i=i+1
         i=0
         for iy in range(-kb1[1],n1y-kb1[1]):
            grid_y_ex[i]=dy*float(iy)+rshift[1]
            i=i+1
         i=0
         for iz in range(-kb1[2],n1z-kb1[2]):
            grid_z_ex[i]=dz*float(iz)+rshift[2]
            i=i+1
         bulkdata_f_ex=np.reshape(bulkdata_ex,(n1x,n1y,n1z),order='C')
         end_si=time()
         print('DATA INTERPOLATE: initialize grid and data    %s'%(str(timedelta(seconds=int(end_si-start_si)))))
         interpolating_function=trispline(grid_x_ex,grid_y_ex,grid_z_ex,bulkdata_f_ex)
      else:         
         end_si=time()
         print('DATA INTERPOLATE: initialize grid and data    %s'%(str(timedelta(seconds=int(end_si-start_si)))))
         interpolating_function=trispline(grid_x,grid_y,grid_z,bulkdata_f)

      end_si=time()
      print('DATA INTERPOLATE: get interpolation function  %s'%(str(timedelta(seconds=int(end_si-start_si)))))
#      print(interpolating_function)
      bulkdata_t=np.zeros(len(grid_t))
      for i in range(len(grid_t)):
         bulkdata_t[i]=interpolating_function.ev(grid_t[i][0],grid_t[i][1],grid_t[i][2])
      end_si=time()
      print('DATA INTERPOLATE: generate data               %s'%(str(timedelta(seconds=int(end_si-start_si)))))
      return bulkdata_t

#################################################
# Write a formated 'density-style' cubefile very similar
# to those created by the gaussian program or the cubegen utility.
# Adapted and modified from Quantum Espresso 6.1/PP/src/cube.f90
# The format is as follows 
#     LINE   FORMAT      CONTENTS
#     ===============================================================
#      1     A           TITLE
#      2     A           DESCRIPTION OF PROPERTY STORED IN CUBEFILE
#      3     I5,3F12.6   #ATOMS, X-,Y-,Z-COORDINATES OF ORIGIN
#      4-6   I5,3F12.6   #GRIDPOINTS, INCREMENT VECTOR
#      #ATOMS LINES OF ATOM COORDINATES:
#      ...   I5,4F12.6   ATOM NUMBER, CHARGE, X-,Y-,Z-COORDINATE
#      REST: 6E13.5      CUBE DATA
#
#     ALL COORDINATES ARE GIVEN IN ATOMIC UNITS.
#####################################################################
def savecube(ofilename,title,nat,x0,nx,ny,nz,at,atm,ityp,r,bulkdata):
   from ase.data import atomic_numbers, atomic_masses
   print('Print data in cube file %s'%(ofilename))
   fout=open(ofilename,'w')
   fout.write('Cubefile created from QE calculation\n')
   if(len(title.split())>0): 
      fout.write(title)   
      if(title.split()[-1] != '\n'): fout.write('\n')
   else:
      fout.write('Contains the selected bulk data\n')
   fout.write(('%5i'+3*'%12.6f'+'\n')%(nat,x0[0],x0[1],x0[2]))
   fout.write(('%5i'+3*'%12.6f'+'\n')%(nx,at[0][0]/float(nx),at[0][1]/float(nx),at[0][2]/float(nx)))
   fout.write(('%5i'+3*'%12.6f'+'\n')%(ny,at[1][0]/float(ny),at[1][1]/float(ny),at[1][2]/float(ny)))
   fout.write(('%5i'+3*'%12.6f'+'\n')%(nz,at[2][0]/float(nz),at[2][1]/float(nz),at[2][2]/float(nz)))

   for ia in range(nat):
      fout.write(('%5i'+4*'%12.6f'+'\n')%(int(atomic_numbers[atm[ityp[ia]-1]]),\
             float(atomic_numbers[atm[ityp[ia]-1]]),r[ia][0],r[ia][1],r[ia][2]))

   i=0
   for ix in range(nx):
      for iy in range(ny): 
         for iz in range(nz):
            fout.write('%13.5e'%(bulkdata[i]))
            i=i+1
            if(i%6==0): fout.write('\n')
 
   fout.close()

#####################################################################

#################################################

"""
#some brief tests

start_grid=time()

data3d=qe3d('Si.ecnl')
print(data3d.title)
print(data3d.atm)
for ia in range(data3d.nat):
   print(data3d.tau[ia],data3d.ityp[ia])
print('read in bulkdata')
print(data3d.bulkdata[0:10])

print(data3d.celldm[0])
at=data3d.get_at()
print(at)
x0=np.zeros(3)
r=data3d.get_rbohr()
savecube('Si.cube',data3d.title,data3d.nat,x0, \
         data3d.nr1x,data3d.nr1y,data3d.nr1z,at,data3d.atm,data3d.ityp,r, \
         data3d.bulkdata)

print('Write cube file Si.cube')
end_grid=time()
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))

data3d=qe3d('N-1.ecnl')
print(data3d.title)
print(data3d.atm)
for ia in range(data3d.nat):
   print(data3d.tau[ia],data3d.ityp[ia])
print('read in bulkdata')
print(data3d.bulkdata[0:10])

print(data3d.celldm[0])
at=data3d.get_at()
print(at)
x0=np.zeros(3)
r=data3d.get_rbohr()
savecube('N-1.cube',data3d.title,data3d.nat,x0, \
         data3d.nr1x,data3d.nr1y,data3d.nr1z,at,data3d.atm,data3d.ityp,r, \
         data3d.bulkdata)

print('Write cube file N-1.cube')
end_grid=time()
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))

vec=([0.,0.,0.,])

grid_t1=(
  [[0.,0.,0.,],
  [1.,1.,1.,],
  [0.2,1.3,-5.0]]

)
bulkdata_t=data3d.shift_interpolate(vec, grid_t1)
print bulkdata_t

end_grid=time()
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))

grid_t=data3d.get_grid()
print('Get grid for cube')
end_grid=time()
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))
bulkdata_t=data3d.shift_interpolate(vec,grid_t)
print('Shift and interpolate')
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))

print('read in bulkdata')
print(data3d.bulkdata[0:10])
savecube('N-1-tricubic.cube',data3d.title,data3d.nat,x0, \
         data3d.nr1x,data3d.nr1y,data3d.nr1z,at,data3d.atm,data3d.ityp,r, \
         bulkdata_t)
end_grid=time()
print('Time elapsed %s'%(str(timedelta(seconds=int(end_grid-start_grid)))))
"""
