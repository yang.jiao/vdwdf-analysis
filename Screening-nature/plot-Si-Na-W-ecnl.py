import numpy as np
import matplotlib.pyplot as pl
from matplotlib import cm
from ase.units import Ry, Bohr
import matplotlib.gridspec as gridspec

linew=2
barw = 1./12.
pts = 8
fonts = 12

fig = pl.figure(figsize=(8,4))
fig.set_facecolor('white')

#########################################

gs = gridspec.GridSpec(3, 5,
                       width_ratios=[1,1,1,1,1],
                       height_ratios=[1,1,1]
                       )

def plot_inset(fpng,ifig,xmin,xmax,ymin,ymax):
   figpng=pl.imread(fpng)
   ax=fig.add_subplot(gs[ifig],aspect=1,zorder=0)
   ax.set_xlim([xmin,xmax])
   ax.set_ylim([ymin,ymax])
   ax.axis('off')
   ax.imshow(figpng)
   return


dataset=(['ecgc-PBE','ecnl-cx','ecnl-pvdW-cx','ecnl-nf-cx','ec-alpha-cx'])
#########################################
#   Si

xmin=10;xmax=1400;ymin=1100;ymax=10

ifig=0
for data in dataset: 
   fpng='./png-vesta-atomcenter-ecutrho800-k8/Si-de-%s-uc.png'%(data)
   plot_inset(fpng,ifig,xmin,xmax,ymin,ymax)
   ifig=ifig+1

#########################################

dataset=(['ecgc-PBE','ecnl-vdW-DF-cx','ecnl-pvdW-vdW-DF-cx','ecnl-nf-vdW-DF-cx','ec-alpha-vdW-DF-cx'])
#########################################
#  Na 

xmin=10;xmax=1400;ymin=1100;ymax=10

ifig=5
for data in dataset: 
   fpng='./png-vesta-atomcenter-ecutrho800-k8/Na-de-%s-uc.png'%(data)
   plot_inset(fpng,ifig,xmin,xmax,ymin,ymax)
   ifig=ifig+1

#########################################
#  W

xmin=10;xmax=1400;ymin=1100;ymax=10

ifig=10
for data in dataset: 
   fpng='./png-vesta-atomcenter-ecutrho800-k24/W-de-%s-uc.png'%(data)
   plot_inset(fpng,ifig,xmin,xmax,ymin,ymax)
   ifig=ifig+1


#########################################

figpng=pl.imread(fpng)
cbmin=np.min(np.array(figpng))
cbmax=np.max(np.array(figpng))
cbar_ax=fig.add_axes([0.1,0.08,0.8,0.015])

gradient = np.linspace(-1,1,256)
gradient = np.vstack((gradient,gradient))
ax1=fig.add_axes([0.4,0.08,0.2,0.01],zorder=-2)
ax1.axis('off')
cax=ax1.imshow(gradient,aspect='auto',cmap=cm.jet)

cbar=pl.colorbar(cax,cax=cbar_ax,orientation='horizontal',cmap=cm.jet,
                ticks=[-1,0,1],extend='both')
cbar.set_ticklabels(["$<-0.1$",'0',"$>0.1$ eV$/\mathrm{\AA}^3$"])
cbar.ax.tick_params(labelsize=fonts)

#Na
fig.text(0.22,0.40,r"$\times 2.5$",fontsize=fonts,ha='right',va='bottom')
fig.text(0.41,0.40,r"$\times 2.5$",fontsize=fonts,ha='right',va='bottom')
fig.text(0.59,0.40,r"$\times 2.5$",fontsize=fonts,ha='right',va='bottom')
fig.text(0.78,0.40,r"$\times 2.5$",fontsize=fonts,ha='right',va='bottom')
fig.text(0.97,0.40,r"$\times 2.5$",fontsize=fonts,ha='right',va='bottom')

#W
fig.text(0.05,0.82,"Si",fontsize=fonts,ha='right',va='center')
fig.text(0.05,0.53,"Na",fontsize=fonts,ha='right',va='center')
fig.text(0.05,0.24,r"W",fontsize=fonts,ha='right',va='center')

fig.text(0.16,0.98,"$\Delta e_c^{PBE}-\Delta e_c^{LDA}$",fontsize=fonts,ha='center',va='top')
fig.text(0.34,0.98,"$\Delta e_c^{nl}$",fontsize=fonts,ha='center',va='top')
fig.text(0.54,0.98,r"$\Delta e_{c,vdW}^{nl}$",fontsize=fonts,ha='center',va='top')
fig.text(0.72,0.98,r"$\Delta e_{c,\alpha}^{nl}$",fontsize=fonts,ha='center',va='top')
fig.text(0.90,0.98,r"$\Delta e_{c,\alpha}$",fontsize=fonts,ha='center',va='top')


fig.tight_layout()
fig.subplots_adjust(left=0.05,right=0.99,top=0.96,bottom=0.12)
fig.subplots_adjust(hspace=0.0,wspace=0.0)
fig.savefig('de_ecnl_Si_Na_W.png',format='png',dpi=600)
pl.show()
