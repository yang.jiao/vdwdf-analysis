
Subfigures in [J. Phys.: Condens. Matter 2020](https://doi.org/10.1088/1361-648X/ab8250) Figure 15 and Figure 16. 
- k-point sampling $`24 \times 24 \times 24`$
