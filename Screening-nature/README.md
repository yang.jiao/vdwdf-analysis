
This directory contains
- Plotting scripts for [J. Phys.: Condens. Matter 2020](https://doi.org/10.1088/1361-648X/ab8250) Figure 15 and Figure 16. 
  - Figure 15: plot-Si-Na-W-xc.py
             de_xc_Si_Na_W.png
  - Figure 16: plot-Si-Na-W-ecnl.py
             de_ecnl_Si_Na_W.png
  - The subfigures are in directories png-vesta-atomcenter-ecutrho800-k8 and png-vesta-atomcenter-ecutrho800-k24.

- Si $`\Delta e_c^{nl}`$ is taken as an example to detailed the subfigure plotting.
![Si-ecnl](png-vesta-atomcenter-ecutrho800-k8/Si-de-ecnl-cx-uc.png)


### Generation of the subfigures

The subfigures present
exchange and correlation contributions to the binding in bulk as described in [J. Phys.: Condens. Matter 2020](https://doi.org/10.1088/1361-648X/ab8250). 
For example in Si
```math
\Delta e_c^{nl} = \sum_i e_c^{nl,atom_i} - e_c^{nl,bulk}
``` 
$`\sum_i`$ runs over atoms which has contribution to the unit cell under consideration.

1. Perform ground state DFT calculation and ACF analysis using [PWscf](https://www.quantum-espresso.org/Doc/INPUT_PW.html) and [PPACF](https://www.quantum-espresso.org/Doc/INPUT_PPACF.html) to get the 3D data files *.ecnl for both bulk Si $`e_c^{nl,bulk}`$ and atom Si $`e_c^{nl,atom}`$.

1. Subtract atom contributions from bulk data. 
The atom data is stored in a cubic super cell. A trispline interpolation was deployed to map $`e_c^{nl,atom}(\tilde{r})`$ on to the same grid as bulk $`e_c^{nl,atom_i}(r)`$. The script [mastersta-bind.py](https://gitlab.com/yang.jiao/vdwdf-analysis/-/tree/master/Screening-nature/3Ddata/mastersta-bind.py)
 will do the interpolation and subtraction. The module to deal with QE 3D data output file is [qe3d.py](https://gitlab.com/yang.jiao/vdwdf-analysis/-/tree/master/Screening-nature/3Ddata/qe3d.py)
It depends on [eqtools](https://pypi.org/project/eqtools/) or the version adapted for Python3 [eqtools3](https://github.com/hajimen/eqtools3).
The output $`\Delta e_c^{nl}`$ is saved in a cube file.

1. Export color map plot using [VESTA](http://jp-minerals.org/vesta/en/). The default unit in QE cube file is Ry/Bohr$`^3`$. The visuallization parameters used in [VESTA](http://jp-minerals.org/vesta/en/) can be saved in *.vesta file. The number following "IMPORT_DENSITY" defines the number of 3D data files to be imported. The following lines define the 3D data files to be used and the weight for each data file. I did the unit conversion here
~~~
IMPORT_DENSITY   1
+91.81576765360094 Si-bind-ecnl-vdW-DF-cx-trispline.cube
~~~


